\documentclass[10pt, a4paper]{article}

%Preambuła dokumentu
\usepackage{graphicx}       % pakiet graficzny, umożliwiający m.in.
                            % import grafik w formacie eps
\usepackage{rotating}       % pakiet umożliwiający obracanie rysunków
\usepackage{subfigure}      % pakiet umożliwiający tworzenie podrysunków
\usepackage{epic}           % pakiet umożliwiający rysowanie w środowisku latex
\usepackage{listings}       % pakiet dedykowany zrodlom programow
\usepackage{verbatim}       % pakiet dedykowany rozmaitym wydrukom tekstowym
\usepackage{amssymb}        % pakiet z rozmaitymi symbolami matematycznymi
\usepackage{amsmath}        % pakiet z rozmaitymi środowiskami matematycznymi
\usepackage[english]{babel}  % pakiet lokalizujący dokument w języku polskim
\usepackage[OT4]{fontenc}
\usepackage[utf8]{inputenc} % w miejsce utf8 można wpisać latin2 bądź cp1250,
                            % w zależności od tego w jaki sposób kodowane są 
                            % polskie znaki diakrytyczne przy wprowadzaniu 
                            % z klawiatury.

\usepackage[draft]{prelim2e}% informacja w stopcje o statusie dokumentu 
                            % (draft-szkic lub final-wersja ostateczna) 

\textwidth      16cm
\textheight     25.5cm
\evensidemargin -3mm
\oddsidemargin  -3mm
\topmargin      -20mm


% deklaracje wymagane przez funkcję drukującą tytuł dokumentu:
%
\author{Łukasz Chojnacki\footnote{Electronics Faculty, Wroclaw University of
    Technology,ul. Z. Janiszewskiego 11/17, 50-320 Wrocław} 
\and 
Tomasz Łacny\footnote{Electronics Faculty, Wroclaw University of
    Technology,ul. Z. Janiszewskiego 11/17, 50-320 Wrocław}
}
 
\title{Physical modelling: report} 

\date{\today}

% Koniec preambuły dokumentu

% Tekst dokumentu

\begin{document}
\maketitle % drukuje tytul, autora i datę zdefiniowaną w preambule
%
%\the\setitem
%

\section{Introduction}
\label{sec:wstep} % definicja etykiety rozdziału
%
In this laboratory exercise we are focused on a \textit{pendulum on a
  cart} in the Robotics Laboratory at the Department of Cybernetics
and Robotics, Wroclaw University of Technology.  The aim of this
laboratory is threefold: analysis of the construction of the
mechanical system designed in of~\textsf{Autodesk Inventor}, automatic
generation of dynamics model of the considered mechanical system in
\textsf{SimMechanics} with use of \textsf{SimMechanics Link}, study of
selected properties of the pendulum on a cart on the basis of the
model in ~\textsf{Matlab} i~\textsf{Simulink}.

\section{Preparation for laboratory classes}
\label{sec:wnw-k}
% for Polish students only!
The calls in the menu \textsl{Akcje} in the main window of the program
work as follows:
\begin{description}
  \item[Otwórz (Open)] -- open the project
  \item[Wpisz (Check in)] -- free the file, share it for others users
  \item[Wypisz (Check out)] -- capture the file, others users cannot edit it at the same time
  \item[Cofnij wypisanie (Undu check out)] -- cancel file changes
  \item[Pobierz poprzednią wersję (download the previous version)] -- download previous version of the file
  \item[Pobierz najnowszą wersję (download the current version)] -- download current version of the file
\end{description}

Action of the user, shown in the Figure entitled \textit{Migawka z
  pracy z Autodesk Vault} is to download the previous version of the file \verb$UchwytZLozyskami.ipt$

There exist analogies in the basic concepts
of construction and use of a revision control system known to you
(e.g. SVN) and Autodesk Vault 2010, for example: multiple users can work on the same project, it is possible to cancel the changes and access to the file from any computer but only one user can edit it at the same time.

\begin{itemize}
\item On the basis of a part's drawing,\\
  \texttt{item\_0026590\_Profil\_8\_160x16\_kolor\_naturalny\_L\_92\_1.ipt}, \\
  I conclude that its geometric and mass parameters are following: 
  
Dimensions 92x16x160 [mm] \\
Mass: 0.344 [kg] \\
Volume : 127082, 989[$mm^3$ ] \\
Center of gravity: (X,Y,Z) = (80,00; 8,964; 46,00) [mm].
\item On the basis on an assembly's drawing
  \texttt{ZespolSilnika.ipn} I conclude that functionality of specific
  components are following: 
  
 \begin{itemize}
\item Silnik - motor moving the carriage
\item Przekładnia silnika - transmission translating the drive
\item Obudowa sprzęgła - protects the clutch
\item Sprzęgło - connects the transmission with the dive
\item Płyta sprzęgła - part of case
\item Łoże silnika - holds the engine
 \end{itemize}
  
\item On the basis of analysis of executive drawings
  \texttt{Uchwyt.idw} I conclude that the technology of manufacturing of
  specific elements is following: turning
\end{itemize}

\section{Physical modeling of a pendulum on a cart in SimMechanics}
\label{sec:mfsm}

The function \texttt{mech\_import} is for importing a Physical Modeling XML file into Physical Modeling.
\texttt{mech\_import} is an element of the toolbox SimMechanics. 
This toolbox includes also the following functions \verb$snnew,sm_lib,smwritevideo,smimport$.

Functions and parameters of the blocks:
\texttt{Body}, \texttt{Ground}, \texttt{Machine Environment}
\texttt{Prismatic}, \texttt{Revolute}
are as follows

\begin{description}
\item[Body] -- represents a user-defined rigid body. Body defined by mass m, inertia tensor I, and coordinate origins and axes for center of gravity (CG) and other user-specified Body coordinate systems
\item[Ground] -- grounds one side of a Joint to a fixed location in the World coordinate system
\item[Machine Environment] --defines the mechanical simulation environment for the machine to which the block is connected: gravity, dimensionality, analysis mode, constraint solver type, tolerances, linearization, and visualization
\item[Prismatic]  -- represents one translational degree of freedom
\item[Revolute] -- represents one rotational degree of freedom
\end{description}

Yes, there are constraints between the pendulum,
the cart and the linear guide in the project \textsf{Autodesk Inventor
  2010} that are being converted to joints in \textsf{SimMechanics}.

Yes, we can observe some relationship between
subassemblies in the drawings: \texttt{ProwadnicaFull.iam},
\texttt{WalZWahadlem.iam}, \texttt{UchwytZWozkiem.iam} in the project
\textsf{Autodesk Inventor 2010} and the blocks in the diagram entitled
\textit{Wirtualne wahadło na wózku: model wahadła w SimMechanics}. 

On the ground of a block diagram it is possible to derive equations of motion for the pendulum on the cart that can take the following form:
\begin{equation}
  \begin{split}
   \dot{x}_1 &= v\\
   \dot{x}_2 &= \varphi + u\\
   \dot{x}_3 &= \omega\\
   \dot{x}_4 &= 2v + u
  \end{split}
\end{equation}

\section{Study of properties of a pendulum on a cart model}

The dynamical system in the visualization window in
remains motionless during animation, in the
course of simulation of the model because remains in equilibrium point.


The functions of blocks \texttt{Joint Initial Condition}, \texttt{Joint
  Actuator}, \texttt{Joint Sensor} are following:
\begin{description}
\item[Joint Initial Condition] -- sets the initial linear/angular position and velocity of some or all of the primitives in a Joint.
\item[Joint Actuator] -- actuates a Joint primitive with generalized force/torque or linear/angular position, velocity, and acceleration motion signals.
\item[Joint Sensor] -- measures linear/angular position, velocity, acceleration, computed force/torque and/or reaction force/torque of a Joint primitive.
\end{description}

The courses of positions, velocities and accelerations in time
for a cart and a pendulum for several different initial conditions are shown in Figure~\ref{fig:sva}
\begin{figure}[hbt]
  \setlength{\unitlength}{1.0cm}
  \centering 
  \subfigure[$x(0)=0$, $\dot{x}(0)=0$, $\theta(0)=0$, $\dot{\theta}(0)=0$]{
    \includegraphics[width=0.3\textwidth]{./graphics/wykres1.jpg}
  }
  \subfigure[$x(0)=0$, $\dot{x}(0)=0$, $\theta(0)=120$, $\dot{\theta}(0)=0$]{
    \includegraphics[width=0.3\textwidth]{./graphics/wykres2.jpg}
  }
  \subfigure[$x(0)=0$, $\dot{x}(0)=0.5$, $\theta(0)=0$, $\dot{\theta}(0)=0$]{
    \includegraphics[width=0.3\textwidth]{./graphics/wykres3.jpg}
  }
  \caption{The courses of positions, velocities and accelerations in time
for a cart and a pendulum for several different initial conditions.}
  \label{fig:sva}
\end{figure}

The equilibrium points for a pendulum on a cart obtained
experimentally are as follows: [0,0,0,0] and [0,0,pi/2,0]

The equilibrium points for a pendulum on a cart can be obtained using
the function \texttt{trim} and the block diagram entitled
\textit{A diagram allowing determination of equilibrium points in Matlab
  for a pendulum on a cart} in the following way:

\verbatiminput{./sources/trim-sesja.out}

The obtained results partially coincide with these that can be obtained experimentally, because has found only one of the two equilibrium points, that has been obtained experimentally.

The matrices of the linear approximation of the model of the pendulum
on the cart in the neighbourhood of the unstable equilibrium point
using the diagram entitled \textit{A diagram allowing determination of
  equilibrium points in Matlab for a pendulum on a cart.} and the
function \texttt{linmod} can be derived as follows.

\verbatiminput{./sources/linmod-sesja.out}

A diagram, which makes possible to compare trajectories of the
nonlinear system and its linear approximation is shown in
Figure~\ref{fig:por}.

 \begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{./graphics/schemat1.pdf}
    \caption{A diagram for comparative analysis of models}
    \label{fig:por}
  \end{center}
 \end{figure}

A neighborhood of the equilibrium point of the nonlinear system such
that the both groups of trajectories are similar can be characterized
as follows:

\section{Control of a pendulum on a cart in a closed-loop system}
\label{sec:swzps}

The diagram of \textit{System decyzyjny} (decision making system) is shown in Figure~\ref{fig:systdecyz}.
 \begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{./graphics/schemat2.jpg}
    \caption{A diagram of \textit{System decyzyjny}. [K = 244.3028, 100.3198, 62.2308, 66.8799]}
    \label{fig:systdecyz}
  \end{center}
 \end{figure}

The courses of positions and velocities in the joints of the pendulum
on a cart during the process of of standing up and stabilisation are
shown in Figure~\ref{fig:xvs2}.

 \begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=0.75\textwidth]{./graphics/wykres4.jpg}
    \caption{Positions and velocities in the joints of the pendulum
      and the cart during the process of standing up and stabilisation
      of the pendulum in the unstable equilibrium point. [$x(0)=0$, $\dot{x}(0)=0$, $\theta(0)=0$, $\dot{\theta}(0)=2$]}
    \label{fig:xvs2}
  \end{center}
 \end{figure}

\end{document}
 
