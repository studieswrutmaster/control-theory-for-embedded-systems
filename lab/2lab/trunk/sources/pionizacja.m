function p=fib(l)
% fib(l) wylicza l-ty element ciagu Fibonacciego,
%        przy czym l jest liczba calkowita nieujemna

if l < 0,
   p=-1;
   error('l < 0 !');
else
   p1=0;
   p2=1;
   for i=2:l, 
      p=p1+p2;
      p1=p2;
      p2=p;
   end;
end;
