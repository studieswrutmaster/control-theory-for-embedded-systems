/*
 * UkladRegulacji.c
 *
 * Real-Time Workshop code generation for Simulink model "UkladRegulacji.mdl".
 *
 * Model version              : 1.2
 * Real-Time Workshop version : 7.5  (R2010a)  25-Jan-2010
 * C source code generated on : Wed Oct 26 18:39:58 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Debugging
 * Validation result: All passed
 */

#include "UkladRegulacji.h"
#include "UkladRegulacji_private.h"

/* Block signals (auto storage) */
BlockIO_UkladRegulacji UkladRegulacji_B;

/* Continuous states */
ContinuousStates_UkladRegulacji UkladRegulacji_X;

/* Block states (auto storage) */
D_Work_UkladRegulacji UkladRegulacji_DWork;

/* Real-time model */
RT_MODEL_UkladRegulacji UkladRegulacji_M_;
RT_MODEL_UkladRegulacji *UkladRegulacji_M = &UkladRegulacji_M_;
static void rate_scheduler(void);

/*
 * This function updates active task flag for each subrate.
 * The function must be called at model base rate, hence the
 * generated code self-manages all its subrates.
 */
static void rate_scheduler(void)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (UkladRegulacji_M->Timing.TaskCounters.TID[2])++;
  if ((UkladRegulacji_M->Timing.TaskCounters.TID[2]) > 29999) {/* Sample time: [30.0s, 0.0s] */
    UkladRegulacji_M->Timing.TaskCounters.TID[2] = 0;
  }

  UkladRegulacji_M->Timing.sampleHits[2] =
    (UkladRegulacji_M->Timing.TaskCounters.TID[2] == 0);
}

/*
 * This function updates continuous states using the ODE5 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE5_A[6] = {
    1.0/5.0, 3.0/10.0, 4.0/5.0, 8.0/9.0, 1.0, 1.0
  };

  static const real_T rt_ODE5_B[6][6] = {
    { 1.0/5.0, 0.0, 0.0, 0.0, 0.0, 0.0 },

    { 3.0/40.0, 9.0/40.0, 0.0, 0.0, 0.0, 0.0 },

    { 44.0/45.0, -56.0/15.0, 32.0/9.0, 0.0, 0.0, 0.0 },

    { 19372.0/6561.0, -25360.0/2187.0, 64448.0/6561.0, -212.0/729.0, 0.0, 0.0 },

    { 9017.0/3168.0, -355.0/33.0, 46732.0/5247.0, 49.0/176.0, -5103.0/18656.0,
      0.0 },

    { 35.0/384.0, 0.0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE5_IntgData *id = (ODE5_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T *f3 = id->f[3];
  real_T *f4 = id->f[4];
  real_T *f5 = id->f[5];
  real_T hB[6];
  int_T i;
  int_T nXc = 4;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  UkladRegulacji_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE5_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[0]);
  rtsiSetdX(si, f1);
  UkladRegulacji_output(0);
  UkladRegulacji_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE5_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[1]);
  rtsiSetdX(si, f2);
  UkladRegulacji_output(0);
  UkladRegulacji_derivatives();

  /* f(:,4) = feval(odefile, t + hA(3), y + f*hB(:,3), args(:)(*)); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE5_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[2]);
  rtsiSetdX(si, f3);
  UkladRegulacji_output(0);
  UkladRegulacji_derivatives();

  /* f(:,5) = feval(odefile, t + hA(4), y + f*hB(:,4), args(:)(*)); */
  for (i = 0; i <= 3; i++) {
    hB[i] = h * rt_ODE5_B[3][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3]);
  }

  rtsiSetT(si, t + h*rt_ODE5_A[3]);
  rtsiSetdX(si, f4);
  UkladRegulacji_output(0);
  UkladRegulacji_derivatives();

  /* f(:,6) = feval(odefile, t + hA(5), y + f*hB(:,5), args(:)(*)); */
  for (i = 0; i <= 4; i++) {
    hB[i] = h * rt_ODE5_B[4][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4]);
  }

  rtsiSetT(si, tnew);
  rtsiSetdX(si, f5);
  UkladRegulacji_output(0);
  UkladRegulacji_derivatives();

  /* tnew = t + hA(6);
     ynew = y + f*hB(:,6); */
  for (i = 0; i <= 5; i++) {
    hB[i] = h * rt_ODE5_B[5][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2] +
                   f3[i]*hB[3] + f4[i]*hB[4] + f5[i]*hB[5]);
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model output function */
void UkladRegulacji_output(int_T tid)
{
  if (rtmIsMajorTimeStep(UkladRegulacji_M)) {
    /* set solver stop time */
    rtsiSetSolverStopTime(&UkladRegulacji_M->solverInfo,
                          ((UkladRegulacji_M->Timing.clockTick0+1)*
      UkladRegulacji_M->Timing.stepSize0));
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(UkladRegulacji_M)) {
    UkladRegulacji_M->Timing.t[0] = rtsiGetT(&UkladRegulacji_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(UkladRegulacji_M) &&
      UkladRegulacji_M->Timing.TaskCounters.TID[2] == 0) {
    /* UnitDelay: '<S2>/Output' */
    UkladRegulacji_B.Output_o = UkladRegulacji_DWork.Output_DSTATE;
  }

  if (rtmIsMajorTimeStep(UkladRegulacji_M) &&
      UkladRegulacji_M->Timing.TaskCounters.TID[1] == 0) {
    /* MultiPortSwitch: '<S1>/Output' incorporates:
     *  Constant: '<S1>/Vector'
     */
    UkladRegulacji_B.Output = UkladRegulacji_P.Vector_Value[(int32_T)
      UkladRegulacji_B.Output_o];
  }

  /* TransferFcn: '<Root>/plant' */
  UkladRegulacji_B.plant = UkladRegulacji_P.plant_C[0]*
    UkladRegulacji_X.plant_CSTATE[0] + UkladRegulacji_P.plant_C[1]*
    UkladRegulacji_X.plant_CSTATE[1];

  /* TransferFcn: '<Root>/controller' */
  UkladRegulacji_B.controller = UkladRegulacji_P.controller_C[0]*
    UkladRegulacji_X.controller_CSTATE[0]
    + UkladRegulacji_P.controller_C[1]*UkladRegulacji_X.controller_CSTATE[1];
  if (rtmIsMajorTimeStep(UkladRegulacji_M) &&
      UkladRegulacji_M->Timing.TaskCounters.TID[1] == 0) {
    /* Scope: '<Root>/Scope' */
    if (rtmIsMajorTimeStep(UkladRegulacji_M)) {
      real_T u[4];
      u[0] = UkladRegulacji_M->Timing.t[1];
      u[1] = UkladRegulacji_B.Output;
      u[2] = UkladRegulacji_B.plant;
      u[3] = UkladRegulacji_B.controller;
      rt_UpdateLogVar((LogVar *)UkladRegulacji_DWork.Scope_PWORK.LoggedData, u,
                      0);
    }
  }

  if (rtmIsMajorTimeStep(UkladRegulacji_M) &&
      UkladRegulacji_M->Timing.TaskCounters.TID[2] == 0) {
    /* Sum: '<S3>/FixPt Sum1' incorporates:
     *  Constant: '<S3>/FixPt Constant'
     */
    UkladRegulacji_B.FixPtSum1 = (uint8_T)(uint32_T)(UkladRegulacji_B.Output_o +
      UkladRegulacji_P.FixPtConstant_Value);

    /* Switch: '<S4>/FixPt Switch' incorporates:
     *  Constant: '<S4>/Constant'
     */
    if (UkladRegulacji_B.FixPtSum1 > UkladRegulacji_P.FixPtSwitch_Threshold) {
      UkladRegulacji_B.FixPtSwitch = UkladRegulacji_P.Constant_Value;
    } else {
      UkladRegulacji_B.FixPtSwitch = UkladRegulacji_B.FixPtSum1;
    }
  }

  /* Sum: '<Root>/Sum' */
  UkladRegulacji_B.Sum = UkladRegulacji_B.Output - UkladRegulacji_B.plant;

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model update function */
void UkladRegulacji_update(int_T tid)
{
  if (rtmIsMajorTimeStep(UkladRegulacji_M) &&
      UkladRegulacji_M->Timing.TaskCounters.TID[2] == 0) {
    /* Update for UnitDelay: '<S2>/Output' */
    UkladRegulacji_DWork.Output_DSTATE = UkladRegulacji_B.FixPtSwitch;
  }

  if (rtmIsMajorTimeStep(UkladRegulacji_M)) {
    rt_ertODEUpdateContinuousStates(&UkladRegulacji_M->solverInfo);
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  ++UkladRegulacji_M->Timing.clockTick0;
  UkladRegulacji_M->Timing.t[0] = rtsiGetSolverStopTime
    (&UkladRegulacji_M->solverInfo);
  if (rtmIsMajorTimeStep(UkladRegulacji_M) &&
      UkladRegulacji_M->Timing.TaskCounters.TID[1] == 0) {
    /* Update absolute timer for sample time: [0.001s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     */
    UkladRegulacji_M->Timing.t[1] =
      (++UkladRegulacji_M->Timing.clockTick1) *
      UkladRegulacji_M->Timing.stepSize1;
  }

  if (rtmIsMajorTimeStep(UkladRegulacji_M) &&
      UkladRegulacji_M->Timing.TaskCounters.TID[2] == 0) {
    /* Update absolute timer for sample time: [30.0s, 0.0s] */
    /* The "clockTick2" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick2"
     * and "Timing.stepSize2". Size of "clockTick2" ensures timer will not
     * overflow during the application lifespan selected.
     */
    UkladRegulacji_M->Timing.t[2] =
      (++UkladRegulacji_M->Timing.clockTick2) *
      UkladRegulacji_M->Timing.stepSize2;
  }

  rate_scheduler();

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Derivatives for root system: '<Root>' */
void UkladRegulacji_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/plant' */
  {
    ((StateDerivatives_UkladRegulacji *) UkladRegulacji_M->ModelData.derivs)
      ->plant_CSTATE[0] = UkladRegulacji_B.controller;
    ((StateDerivatives_UkladRegulacji *) UkladRegulacji_M->ModelData.derivs)
      ->plant_CSTATE[0] += (UkladRegulacji_P.plant_A[0])*
      UkladRegulacji_X.plant_CSTATE[0]
      + (UkladRegulacji_P.plant_A[1])*UkladRegulacji_X.plant_CSTATE[1];
    ((StateDerivatives_UkladRegulacji *) UkladRegulacji_M->ModelData.derivs)
      ->plant_CSTATE[1]= UkladRegulacji_X.plant_CSTATE[0];
  }

  /* Derivatives for TransferFcn: '<Root>/controller' */
  {
    ((StateDerivatives_UkladRegulacji *) UkladRegulacji_M->ModelData.derivs)
      ->controller_CSTATE[0] = UkladRegulacji_B.Sum;
    ((StateDerivatives_UkladRegulacji *) UkladRegulacji_M->ModelData.derivs)
      ->controller_CSTATE[0] += (UkladRegulacji_P.controller_A[0])*
      UkladRegulacji_X.controller_CSTATE[0]
      + (UkladRegulacji_P.controller_A[1])*UkladRegulacji_X.controller_CSTATE[1];
    ((StateDerivatives_UkladRegulacji *) UkladRegulacji_M->ModelData.derivs)
      ->controller_CSTATE[1]= UkladRegulacji_X.controller_CSTATE[0];
  }
}

/* Model initialize function */
void UkladRegulacji_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)UkladRegulacji_M, 0,
                sizeof(RT_MODEL_UkladRegulacji));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&UkladRegulacji_M->solverInfo,
                          &UkladRegulacji_M->Timing.simTimeStep);
    rtsiSetTPtr(&UkladRegulacji_M->solverInfo, &rtmGetTPtr(UkladRegulacji_M));
    rtsiSetStepSizePtr(&UkladRegulacji_M->solverInfo,
                       &UkladRegulacji_M->Timing.stepSize0);
    rtsiSetdXPtr(&UkladRegulacji_M->solverInfo,
                 &UkladRegulacji_M->ModelData.derivs);
    rtsiSetContStatesPtr(&UkladRegulacji_M->solverInfo,
                         &UkladRegulacji_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&UkladRegulacji_M->solverInfo,
      &UkladRegulacji_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&UkladRegulacji_M->solverInfo, (&rtmGetErrorStatus
      (UkladRegulacji_M)));
    rtsiSetRTModelPtr(&UkladRegulacji_M->solverInfo, UkladRegulacji_M);
  }

  rtsiSetSimTimeStep(&UkladRegulacji_M->solverInfo, MAJOR_TIME_STEP);
  UkladRegulacji_M->ModelData.intgData.y = UkladRegulacji_M->ModelData.odeY;
  UkladRegulacji_M->ModelData.intgData.f[0] = UkladRegulacji_M->ModelData.odeF[0];
  UkladRegulacji_M->ModelData.intgData.f[1] = UkladRegulacji_M->ModelData.odeF[1];
  UkladRegulacji_M->ModelData.intgData.f[2] = UkladRegulacji_M->ModelData.odeF[2];
  UkladRegulacji_M->ModelData.intgData.f[3] = UkladRegulacji_M->ModelData.odeF[3];
  UkladRegulacji_M->ModelData.intgData.f[4] = UkladRegulacji_M->ModelData.odeF[4];
  UkladRegulacji_M->ModelData.intgData.f[5] = UkladRegulacji_M->ModelData.odeF[5];
  UkladRegulacji_M->ModelData.contStates = ((real_T *) &UkladRegulacji_X);
  rtsiSetSolverData(&UkladRegulacji_M->solverInfo, (void *)
                    &UkladRegulacji_M->ModelData.intgData);
  rtsiSetSolverName(&UkladRegulacji_M->solverInfo,"ode5");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = UkladRegulacji_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    mdlTsMap[2] = 2;
    UkladRegulacji_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    UkladRegulacji_M->Timing.sampleTimes =
      (&UkladRegulacji_M->Timing.sampleTimesArray[0]);
    UkladRegulacji_M->Timing.offsetTimes =
      (&UkladRegulacji_M->Timing.offsetTimesArray[0]);

    /* task periods */
    UkladRegulacji_M->Timing.sampleTimes[0] = (0.0);
    UkladRegulacji_M->Timing.sampleTimes[1] = (0.001);
    UkladRegulacji_M->Timing.sampleTimes[2] = (30.0);

    /* task offsets */
    UkladRegulacji_M->Timing.offsetTimes[0] = (0.0);
    UkladRegulacji_M->Timing.offsetTimes[1] = (0.0);
    UkladRegulacji_M->Timing.offsetTimes[2] = (0.0);
  }

  rtmSetTPtr(UkladRegulacji_M, &UkladRegulacji_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = UkladRegulacji_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    mdlSampleHits[2] = 1;
    UkladRegulacji_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(UkladRegulacji_M, 150.0);
  UkladRegulacji_M->Timing.stepSize0 = 0.001;
  UkladRegulacji_M->Timing.stepSize1 = 0.001;
  UkladRegulacji_M->Timing.stepSize2 = 30.0;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    UkladRegulacji_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(UkladRegulacji_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(UkladRegulacji_M->rtwLogInfo, (NULL));
    rtliSetLogT(UkladRegulacji_M->rtwLogInfo, "tout");
    rtliSetLogX(UkladRegulacji_M->rtwLogInfo, "");
    rtliSetLogXFinal(UkladRegulacji_M->rtwLogInfo, "");
    rtliSetSigLog(UkladRegulacji_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(UkladRegulacji_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(UkladRegulacji_M->rtwLogInfo, 0);
    rtliSetLogMaxRows(UkladRegulacji_M->rtwLogInfo, 5000);
    rtliSetLogDecimation(UkladRegulacji_M->rtwLogInfo, 1);
    rtliSetLogY(UkladRegulacji_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(UkladRegulacji_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(UkladRegulacji_M->rtwLogInfo, (NULL));
  }

  UkladRegulacji_M->solverInfoPtr = (&UkladRegulacji_M->solverInfo);
  UkladRegulacji_M->Timing.stepSize = (0.001);
  rtsiSetFixedStepSize(&UkladRegulacji_M->solverInfo, 0.001);
  rtsiSetSolverMode(&UkladRegulacji_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  UkladRegulacji_M->ModelData.blockIO = ((void *) &UkladRegulacji_B);
  (void) memset(((void *) &UkladRegulacji_B), 0,
                sizeof(BlockIO_UkladRegulacji));

  /* parameters */
  UkladRegulacji_M->ModelData.defaultParam = ((real_T *)&UkladRegulacji_P);

  /* states (continuous) */
  {
    real_T *x = (real_T *) &UkladRegulacji_X;
    UkladRegulacji_M->ModelData.contStates = (x);
    (void) memset((void *)&UkladRegulacji_X, 0,
                  sizeof(ContinuousStates_UkladRegulacji));
  }

  /* states (dwork) */
  UkladRegulacji_M->Work.dwork = ((void *) &UkladRegulacji_DWork);
  (void) memset((void *)&UkladRegulacji_DWork, 0,
                sizeof(D_Work_UkladRegulacji));
}

/* Model terminate function */
void UkladRegulacji_terminate(void)
{
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/

/* Solver interface called by GRT_Main */
#ifndef USE_GENERATED_SOLVER

void rt_ODECreateIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEDestroyIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEUpdateContinuousStates(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

#endif

void MdlOutputs(int_T tid)
{
  UkladRegulacji_output(tid);
}

void MdlUpdate(int_T tid)
{
  UkladRegulacji_update(tid);
}

void MdlInitializeSizes(void)
{
  UkladRegulacji_M->Sizes.numContStates = (4);/* Number of continuous states */
  UkladRegulacji_M->Sizes.numY = (0);  /* Number of model outputs */
  UkladRegulacji_M->Sizes.numU = (0);  /* Number of model inputs */
  UkladRegulacji_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  UkladRegulacji_M->Sizes.numSampTimes = (3);/* Number of sample times */
  UkladRegulacji_M->Sizes.numBlocks = (12);/* Number of blocks */
  UkladRegulacji_M->Sizes.numBlockIO = (7);/* Number of block outputs */
  UkladRegulacji_M->Sizes.numBlockPrms = (17);/* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
  /* InitializeConditions for UnitDelay: '<S2>/Output' */
  UkladRegulacji_DWork.Output_DSTATE = UkladRegulacji_P.Output_X0;

  /* InitializeConditions for TransferFcn: '<Root>/plant' */
  UkladRegulacji_X.plant_CSTATE[0] = 0.0;
  UkladRegulacji_X.plant_CSTATE[1] = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/controller' */
  UkladRegulacji_X.controller_CSTATE[0] = 0.0;
  UkladRegulacji_X.controller_CSTATE[1] = 0.0;
}

void MdlStart(void)
{
  /* Start for UnitDelay: '<S2>/Output' */
  UkladRegulacji_B.Output_o = UkladRegulacji_P.Output_X0;

  /* Start for Scope: '<Root>/Scope' */
  {
    int_T numCols = 4;
    UkladRegulacji_DWork.Scope_PWORK.LoggedData = rt_CreateLogVar(
      UkladRegulacji_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(UkladRegulacji_M),
      UkladRegulacji_M->Timing.stepSize0,
      (&rtmGetErrorStatus(UkladRegulacji_M)),
      "y_yref",
      SS_DOUBLE,
      0,
      0,
      0,
      4,
      1,
      (int_T *)&numCols,
      NO_LOGVALDIMS,
      (NULL),
      0,
      1,
      0.001,
      1);
    if (UkladRegulacji_DWork.Scope_PWORK.LoggedData == (NULL))
      return;
  }

  MdlInitialize();
}

void MdlTerminate(void)
{
  UkladRegulacji_terminate();
}

RT_MODEL_UkladRegulacji *UkladRegulacji(void)
{
  UkladRegulacji_initialize(1);
  return UkladRegulacji_M;
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/
