/*
 * UkladRegulacji_data.c
 *
 * Real-Time Workshop code generation for Simulink model "UkladRegulacji.mdl".
 *
 * Model version              : 1.2
 * Real-Time Workshop version : 7.5  (R2010a)  25-Jan-2010
 * C source code generated on : Wed Oct 26 18:39:58 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Debugging
 * Validation result: All passed
 */

#include "UkladRegulacji.h"
#include "UkladRegulacji_private.h"

/* Block parameters (auto storage) */
Parameters_UkladRegulacji UkladRegulacji_P = {
  /*  Expression: OutValues
   * Referenced by: '<S1>/Vector'
   */
  { 2000.0, 1000.0, 3000.0, 2000.0, 1000.0 },

  /*  Computed Parameter: plant_A
   * Referenced by: '<Root>/plant'
   */
  { -1.6142857142857142E+001, -4.7619047619047613E+001 },

  /*  Computed Parameter: plant_C
   * Referenced by: '<Root>/plant'
   */
  { 0.0, 6.9047619047619046E+003 },

  /*  Computed Parameter: controller_A
   * Referenced by: '<Root>/controller'
   */
  { -3.3333333333333336E+001, -0.0 },

  /*  Computed Parameter: controller_C
   * Referenced by: '<Root>/controller'
   */
  { 0.0, 0.2 },
  0U,                                  /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S4>/Constant'
                                        */
  0U,                                  /* Computed Parameter: Output_X0
                                        * Referenced by: '<S2>/Output'
                                        */
  1U,                                  /* Computed Parameter: FixPtConstant_Value
                                        * Referenced by: '<S3>/FixPt Constant'
                                        */
  4U                                   /* Computed Parameter: FixPtSwitch_Threshold
                                        * Referenced by: '<S4>/FixPt Switch'
                                        */
};
