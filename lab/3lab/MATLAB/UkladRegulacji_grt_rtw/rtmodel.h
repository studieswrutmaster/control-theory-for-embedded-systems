/*
 *  rtmodel.h:
 *
 * Real-Time Workshop code generation for Simulink model "UkladRegulacji.mdl".
 *
 * Model version              : 1.2
 * Real-Time Workshop version : 7.5  (R2010a)  25-Jan-2010
 * C source code generated on : Wed Oct 26 18:39:58 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Debugging
 * Validation result: All passed
 */
#ifndef RTW_HEADER_rtmodel_h_
#define RTW_HEADER_rtmodel_h_

/*
 *  Includes the appropriate headers when we are using rtModel
 */
#include "UkladRegulacji.h"
#endif                                 /* RTW_HEADER_rtmodel_h_ */
