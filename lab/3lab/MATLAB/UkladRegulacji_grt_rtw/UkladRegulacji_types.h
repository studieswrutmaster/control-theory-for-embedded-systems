/*
 * UkladRegulacji_types.h
 *
 * Real-Time Workshop code generation for Simulink model "UkladRegulacji.mdl".
 *
 * Model version              : 1.2
 * Real-Time Workshop version : 7.5  (R2010a)  25-Jan-2010
 * C source code generated on : Wed Oct 26 18:39:58 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86/Pentium
 * Code generation objective: Debugging
 * Validation result: All passed
 */
#ifndef RTW_HEADER_UkladRegulacji_types_h_
#define RTW_HEADER_UkladRegulacji_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_UkladRegulacji_ Parameters_UkladRegulacji;

/* Forward declaration for rtModel */
typedef struct RT_MODEL_UkladRegulacji RT_MODEL_UkladRegulacji;

#endif                                 /* RTW_HEADER_UkladRegulacji_types_h_ */
