clc; close all;

method = 'lqr'

name = 'simulation';


figure(1)
plot(simPozycjaWozka.time,simPozycjaWozka.signals.values)
xlabel('seconds')
ylabel('position')
axis([0 10 -1 1])
title(sprintf('position of a cart (%s) %s',name,method))
grid

figure(2)
plot(simPozycjaWahadla.time,simPozycjaWahadla.signals.values)
xlabel('seconds')
ylabel('position')
title(sprintf('position of a pendulum (%s) %s',name,method))
axis([0 10 -5 5])
grid

figure(3)
plot(simPredkoscWozka.time,simPredkoscWozka.signals.values)
xlabel('seconds')
ylabel('velocity')
title(sprintf('velocity of a cart (%s) %s',name,method))
axis([0 10 -5 5])
grid

figure(4)
plot(simPredkoscWahadla.time,simPredkoscWahadla.signals.values)
xlabel('seconds')
ylabel('velocity')
title(sprintf('velocity of a pendulum (%s) %s',name,method))
axis([0 10 -10 10])
grid

figure(5)
plot(simSila.time,simSila.signals.values)
xlabel('seconds')
ylabel('force [N]')
axis([0 10 -35 35])
title(sprintf('force (%s) %s',name,method))
grid


% name = 'experiment';
% 
% 
% figure(6)
% plot(pozycjaWozka.time,pozycjaWozka.signals.values)
% xlabel('seconds')
% ylabel('position')
% axis([0 150 -1 1])
% title(sprintf('position of a cart (%s) %s',name,method))
% grid
% 
% figure(7)
% plot(pozycjaWahadla.time,pozycjaWahadla.signals.values)
% xlabel('seconds')
% ylabel('position')
% axis([0 150 -5 5])
% title(sprintf('position of a pendulum (%s) %s',name,method))
% grid
% 
% figure(8)
% plot(predkoscWozka.time,predkoscWozka.signals.values)
% xlabel('seconds')
% ylabel('velocity')
% axis([0 150 -5 5])
% title(sprintf('velocity of a cart (%s) %s',name,method))
% grid
% 
% figure(9)
% plot(predkoscWahadla.time,predkoscWahadla.signals.values)
% xlabel('seconds')
% ylabel('velocity')
% axis([0 150 -10 10])
% title(sprintf('velocity of a pendulum (%s) %s',name,method))
% grid
% 
% figure(10)
% plot(sila.time,sila.signals.values)
% xlabel('seconds')
% ylabel('force [N]')
% axis([0 150 -35 35])
% title(sprintf('force (%s) %s',name,method))
% grid
