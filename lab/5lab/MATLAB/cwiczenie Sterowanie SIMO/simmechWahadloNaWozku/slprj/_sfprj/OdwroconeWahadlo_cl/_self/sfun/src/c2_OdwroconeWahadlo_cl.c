/* Include files */

#include "blascompat32.h"
#include "OdwroconeWahadlo_cl_sfun.h"
#include "c2_OdwroconeWahadlo_cl.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "OdwroconeWahadlo_cl_sfun_debug_macros.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
static const char *c2_debug_family_names[7] = { "xMax", "xMax2", "f", "nargin",
  "nargout", "u", "y" };

/* Function Declarations */
static void initialize_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance);
static void initialize_params_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance);
static void enable_c2_OdwroconeWahadlo_cl(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance);
static void disable_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance);
static void c2_update_debugger_state_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance);
static void set_sim_state_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance, const mxArray *c2_st);
static void finalize_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance);
static void sf_c2_OdwroconeWahadlo_cl(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber);
static const mxArray *c2_sf_marshall(void *chartInstanceVoid, void *c2_u);
static const mxArray *c2_b_sf_marshall(void *chartInstanceVoid, void *c2_u);
static const mxArray *c2_c_sf_marshall(void *chartInstanceVoid, void *c2_u);
static const mxArray *c2_d_sf_marshall(void *chartInstanceVoid, void *c2_u);
static real_T c2_emlrt_marshallIn(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance, const mxArray *c2_y, const char_T *c2_name);
static uint8_T c2_b_emlrt_marshallIn(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance, const mxArray *c2_b_is_active_c2_OdwroconeWahadlo_cl, const
  char_T *c2_name);
static void init_dsm_address_info(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance)
{
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
  chartInstance->c2_is_active_c2_OdwroconeWahadlo_cl = 0U;
}

static void initialize_params_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance)
{
}

static void enable_c2_OdwroconeWahadlo_cl(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance)
{
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
}

static void disable_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance)
{
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
}

static void c2_update_debugger_state_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance)
{
}

static const mxArray *get_sim_state_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance)
{
  const mxArray *c2_st = NULL;
  const mxArray *c2_y = NULL;
  real_T c2_hoistedGlobal;
  real_T c2_u;
  const mxArray *c2_b_y = NULL;
  uint8_T c2_b_hoistedGlobal;
  uint8_T c2_b_u;
  const mxArray *c2_c_y = NULL;
  real_T *c2_d_y;
  c2_d_y = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c2_st = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_createcellarray(2));
  c2_hoistedGlobal = *c2_d_y;
  c2_u = c2_hoistedGlobal;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_u, 0, 0U, 0U, 0U, 0));
  sf_mex_setcell(c2_y, 0, c2_b_y);
  c2_b_hoistedGlobal = chartInstance->c2_is_active_c2_OdwroconeWahadlo_cl;
  c2_b_u = c2_b_hoistedGlobal;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_b_u, 3, 0U, 0U, 0U, 0));
  sf_mex_setcell(c2_y, 1, c2_c_y);
  sf_mex_assign(&c2_st, c2_y);
  return c2_st;
}

static void set_sim_state_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance, const mxArray *c2_st)
{
  const mxArray *c2_u;
  real_T *c2_y;
  c2_y = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  chartInstance->c2_doneDoubleBufferReInit = TRUE;
  c2_u = sf_mex_dup(c2_st);
  *c2_y = c2_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c2_u, 0)),
    "y");
  chartInstance->c2_is_active_c2_OdwroconeWahadlo_cl = c2_b_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c2_u, 1)),
     "is_active_c2_OdwroconeWahadlo_cl");
  sf_mex_destroy(&c2_u);
  c2_update_debugger_state_c2_OdwroconeWahadlo_cl(chartInstance);
  sf_mex_destroy(&c2_st);
}

static void finalize_c2_OdwroconeWahadlo_cl
  (SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance)
{
}

static void sf_c2_OdwroconeWahadlo_cl(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance)
{
  int32_T c2_i0;
  int32_T c2_previousEvent;
  int32_T c2_i1;
  real_T c2_hoistedGlobal[5];
  int32_T c2_i2;
  real_T c2_u[5];
  uint32_T c2_debug_family_var_map[7];
  static const char *c2_sv0[7] = { "xMax", "xMax2", "f", "nargin", "nargout",
    "u", "y" };

  real_T c2_xMax;
  real_T c2_xMax2;
  real_T c2_f[10];
  real_T c2_nargin = 1.0;
  real_T c2_nargout = 1.0;
  real_T c2_y;
  int32_T c2_i3;
  static real_T c2_dv0[10] = { 2.0, 2.0, -2.0, 0.0, -6.0, -6.0, 8.5, 11.0, -10.0,
    -10.0 };

  real_T c2_x;
  real_T c2_b_x;
  real_T c2_c_x;
  real_T c2_d_x;
  real_T c2_e_x;
  real_T c2_f_x;
  real_T *c2_b_y;
  real_T (*c2_b_u)[5];
  c2_b_y = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c2_b_u = (real_T (*)[5])ssGetInputPortSignal(chartInstance->S, 0);
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG,0);
  for (c2_i0 = 0; c2_i0 < 5; c2_i0 = c2_i0 + 1) {
    _SFD_DATA_RANGE_CHECK((*c2_b_u)[c2_i0], 0U);
  }

  _SFD_DATA_RANGE_CHECK(*c2_b_y, 1U);
  c2_previousEvent = _sfEvent_;
  _sfEvent_ = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG,0);
  for (c2_i1 = 0; c2_i1 < 5; c2_i1 = c2_i1 + 1) {
    c2_hoistedGlobal[c2_i1] = (*c2_b_u)[c2_i1];
  }

  for (c2_i2 = 0; c2_i2 < 5; c2_i2 = c2_i2 + 1) {
    c2_u[c2_i2] = c2_hoistedGlobal[c2_i2];
  }

  sf_debug_symbol_scope_push_eml(0U, 7U, 7U, c2_sv0, c2_debug_family_var_map);
  sf_debug_symbol_scope_add_eml(&c2_xMax, c2_sf_marshall, 0U);
  sf_debug_symbol_scope_add_eml(&c2_xMax2, c2_sf_marshall, 1U);
  sf_debug_symbol_scope_add_eml(&c2_f, c2_c_sf_marshall, 2U);
  sf_debug_symbol_scope_add_eml(&c2_nargin, c2_sf_marshall, 3U);
  sf_debug_symbol_scope_add_eml(&c2_nargout, c2_sf_marshall, 4U);
  sf_debug_symbol_scope_add_eml(&c2_u, c2_b_sf_marshall, 5U);
  sf_debug_symbol_scope_add_eml(&c2_y, c2_sf_marshall, 6U);
  CV_EML_FCN(0, 0);

  /*  standUp : implementacja algorytmu podnoszenia wahadla.  */
  /*  */
  /*  u(1) - pozycja wahadla */
  /*  u(2) - pozycja wozka */
  /*  u(3) - predkosc katowa wahadla */
  /*  u(4) - predkosc liniowa wozka */
  /*  u(5) - czas */
  /*  y - sila */
  /*  */
  _SFD_EML_CALL(0,11);
  c2_xMax = 0.5;

  /*  maksymalne pozadane odchylenie pozycji wozka od srodka toru */
  _SFD_EML_CALL(0,12);
  c2_xMax2 = 0.25;
  _SFD_EML_CALL(0,14);
  for (c2_i3 = 0; c2_i3 < 10; c2_i3 = c2_i3 + 1) {
    c2_f[c2_i3] = c2_dv0[c2_i3];
  }

  /*  tablica kolejno zadawanych wartosci sil */
  _SFD_EML_CALL(0,16);
  if (CV_EML_IF(0, 0, c2_u[4] < 0.2)) {
    _SFD_EML_CALL(0,17);
    c2_y = c2_f[0];
  } else {
    _SFD_EML_CALL(0,19);
    if (CV_EML_COND(0, 0, c2_u[1] > 0.0)) {
      if (CV_EML_COND(0, 1, c2_u[3] > 0.0)) {
        if (CV_EML_COND(0, 2, c2_u[1] <= c2_xMax2)) {
          CV_EML_MCDC(0, 0, TRUE);
          CV_EML_IF(0, 1, TRUE);
          _SFD_EML_CALL(0,20);
          c2_y = c2_f[1];
          goto label_1;
        } else {
          goto label_2;
        }
      }
    }

   label_2:
    ;
    CV_EML_MCDC(0, 0, FALSE);
    CV_EML_IF(0, 1, FALSE);
    _SFD_EML_CALL(0,22);
    if (CV_EML_COND(0, 3, c2_u[1] > 0.0)) {
      if (CV_EML_COND(0, 4, c2_u[3] > 0.0)) {
        if (CV_EML_COND(0, 5, c2_xMax2 < c2_u[1])) {
          if (CV_EML_COND(0, 6, c2_u[1] <= c2_xMax)) {
            CV_EML_MCDC(0, 1, TRUE);
            CV_EML_IF(0, 2, TRUE);
            _SFD_EML_CALL(0,23);
            c2_y = c2_f[2];
            goto label_3;
          } else {
            goto label_4;
          }
        } else {
          goto label_5;
        }
      }
    }

   label_5:
    ;
   label_4:
    ;
    CV_EML_MCDC(0, 1, FALSE);
    CV_EML_IF(0, 2, FALSE);
    _SFD_EML_CALL(0,25);
    if (CV_EML_COND(0, 7, c2_u[1] > c2_xMax)) {
      if (CV_EML_COND(0, 8, c2_u[2] < 0.0)) {
        CV_EML_MCDC(0, 2, TRUE);
        CV_EML_IF(0, 3, TRUE);
        _SFD_EML_CALL(0,26);
        c2_y = c2_f[3];
        goto label_6;
      }
    }

    CV_EML_MCDC(0, 2, FALSE);
    CV_EML_IF(0, 3, FALSE);
    _SFD_EML_CALL(0,28);
    if (CV_EML_COND(0, 9, c2_u[1] > 0.0)) {
      if (CV_EML_COND(0, 10, c2_u[3] <= 0.0)) {
        CV_EML_MCDC(0, 3, TRUE);
        CV_EML_IF(0, 4, TRUE);
        _SFD_EML_CALL(0,29);
        c2_y = c2_f[4];
        goto label_7;
      }
    }

    CV_EML_MCDC(0, 3, FALSE);
    CV_EML_IF(0, 4, FALSE);
    _SFD_EML_CALL(0,31);
    if (CV_EML_COND(0, 11, c2_u[1] < 0.0)) {
      if (CV_EML_COND(0, 12, c2_u[3] < 0.0)) {
        if (CV_EML_COND(0, 13, c2_u[1] >= -c2_xMax2)) {
          CV_EML_MCDC(0, 4, TRUE);
          CV_EML_IF(0, 5, TRUE);
          _SFD_EML_CALL(0,32);
          c2_y = c2_f[5];
          goto label_8;
        } else {
          goto label_9;
        }
      }
    }

   label_9:
    ;
    CV_EML_MCDC(0, 4, FALSE);
    CV_EML_IF(0, 5, FALSE);
    _SFD_EML_CALL(0,34);
    if (CV_EML_COND(0, 14, c2_u[1] < 0.0)) {
      if (CV_EML_COND(0, 15, c2_u[3] < 0.0)) {
        if (CV_EML_COND(0, 16, c2_u[1] < -c2_xMax2)) {
          if (CV_EML_COND(0, 17, -c2_xMax <= c2_u[1])) {
            CV_EML_MCDC(0, 5, TRUE);
            CV_EML_IF(0, 6, TRUE);
            _SFD_EML_CALL(0,36);
            c2_y = c2_f[6];

            /* 8.5; */
            goto label_10;
          } else {
            goto label_11;
          }
        } else {
          goto label_12;
        }
      }
    }

   label_12:
    ;
   label_11:
    ;
    CV_EML_MCDC(0, 5, FALSE);
    CV_EML_IF(0, 6, FALSE);
    _SFD_EML_CALL(0,38);
    if (CV_EML_COND(0, 18, c2_u[2] > 0.0)) {
      c2_x = c2_u[0];
      c2_b_x = c2_x;
      c2_c_x = c2_b_x;
      c2_b_x = c2_c_x;
      c2_b_x = muDoubleScalarCos(c2_b_x);
      if (CV_EML_COND(0, 19, c2_b_x < 0.0)) {
        CV_EML_MCDC(0, 6, TRUE);
        CV_EML_IF(0, 7, TRUE);
        _SFD_EML_CALL(0,39);
        c2_y = c2_f[7];

        /* 11; */
        goto label_13;
      }
    }

    CV_EML_MCDC(0, 6, FALSE);
    CV_EML_IF(0, 7, FALSE);
    _SFD_EML_CALL(0,41);
    if (CV_EML_COND(0, 20, c2_u[2] > 0.0)) {
      c2_d_x = c2_u[0];
      c2_e_x = c2_d_x;
      c2_f_x = c2_e_x;
      c2_e_x = c2_f_x;
      c2_e_x = muDoubleScalarCos(c2_e_x);
      if (CV_EML_COND(0, 21, c2_e_x >= 0.0)) {
        CV_EML_MCDC(0, 7, TRUE);
        CV_EML_IF(0, 8, TRUE);
        _SFD_EML_CALL(0,42);
        c2_y = c2_f[8];

        /* -10; */
        goto label_14;
      }
    }

    CV_EML_MCDC(0, 7, FALSE);
    CV_EML_IF(0, 8, FALSE);
    _SFD_EML_CALL(0,44);
    c2_y = c2_f[9];

    /* -10; */
   label_14:
    ;
   label_13:
    ;
   label_10:
    ;
   label_8:
    ;
   label_7:
    ;
   label_6:
    ;
   label_3:
    ;
   label_1:
    ;
  }

  _SFD_EML_CALL(0,-44);
  sf_debug_symbol_scope_pop();
  *c2_b_y = c2_y;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
  _sfEvent_ = c2_previousEvent;
  sf_debug_check_for_state_inconsistency(_OdwroconeWahadlo_clMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber
    );
}

static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber)
{
}

static const mxArray *c2_sf_marshall(void *chartInstanceVoid, void *c2_u)
{
  const mxArray *c2_y = NULL;
  real_T c2_b_u;
  const mxArray *c2_b_y = NULL;
  SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance;
  chartInstance = (SFc2_OdwroconeWahadlo_clInstanceStruct *)chartInstanceVoid;
  c2_y = NULL;
  c2_b_u = *((real_T *)c2_u);
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_u, 0, 0U, 0U, 0U, 0));
  sf_mex_assign(&c2_y, c2_b_y);
  return c2_y;
}

static const mxArray *c2_b_sf_marshall(void *chartInstanceVoid, void *c2_u)
{
  const mxArray *c2_y = NULL;
  int32_T c2_i4;
  real_T c2_b_u[5];
  const mxArray *c2_b_y = NULL;
  SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance;
  chartInstance = (SFc2_OdwroconeWahadlo_clInstanceStruct *)chartInstanceVoid;
  c2_y = NULL;
  for (c2_i4 = 0; c2_i4 < 5; c2_i4 = c2_i4 + 1) {
    c2_b_u[c2_i4] = (*((real_T (*)[5])c2_u))[c2_i4];
  }

  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_u, 0, 0U, 1U, 0U, 1, 5));
  sf_mex_assign(&c2_y, c2_b_y);
  return c2_y;
}

static const mxArray *c2_c_sf_marshall(void *chartInstanceVoid, void *c2_u)
{
  const mxArray *c2_y = NULL;
  int32_T c2_i5;
  real_T c2_b_u[10];
  const mxArray *c2_b_y = NULL;
  SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance;
  chartInstance = (SFc2_OdwroconeWahadlo_clInstanceStruct *)chartInstanceVoid;
  c2_y = NULL;
  for (c2_i5 = 0; c2_i5 < 10; c2_i5 = c2_i5 + 1) {
    c2_b_u[c2_i5] = (*((real_T (*)[10])c2_u))[c2_i5];
  }

  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_u, 0, 0U, 1U, 0U, 2, 1, 10));
  sf_mex_assign(&c2_y, c2_b_y);
  return c2_y;
}

const mxArray *sf_c2_OdwroconeWahadlo_cl_get_eml_resolved_functions_info(void)
{
  const mxArray *c2_nameCaptureInfo = NULL;
  c2_ResolvedFunctionInfo c2_info[10];
  c2_ResolvedFunctionInfo (*c2_b_info)[10];
  const mxArray *c2_m0 = NULL;
  int32_T c2_i6;
  c2_ResolvedFunctionInfo *c2_r0;
  c2_nameCaptureInfo = NULL;
  c2_b_info = (c2_ResolvedFunctionInfo (*)[10])c2_info;
  (*c2_b_info)[0].context = "";
  (*c2_b_info)[0].name = "uminus";
  (*c2_b_info)[0].dominantType = "double";
  (*c2_b_info)[0].resolved = "[B]uminus";
  (*c2_b_info)[0].fileLength = 0U;
  (*c2_b_info)[0].fileTime1 = 0U;
  (*c2_b_info)[0].fileTime2 = 0U;
  (*c2_b_info)[1].context = "";
  (*c2_b_info)[1].name = "lt";
  (*c2_b_info)[1].dominantType = "double";
  (*c2_b_info)[1].resolved = "[B]lt";
  (*c2_b_info)[1].fileLength = 0U;
  (*c2_b_info)[1].fileTime1 = 0U;
  (*c2_b_info)[1].fileTime2 = 0U;
  (*c2_b_info)[2].context = "";
  (*c2_b_info)[2].name = "gt";
  (*c2_b_info)[2].dominantType = "double";
  (*c2_b_info)[2].resolved = "[B]gt";
  (*c2_b_info)[2].fileLength = 0U;
  (*c2_b_info)[2].fileTime1 = 0U;
  (*c2_b_info)[2].fileTime2 = 0U;
  (*c2_b_info)[3].context = "";
  (*c2_b_info)[3].name = "le";
  (*c2_b_info)[3].dominantType = "double";
  (*c2_b_info)[3].resolved = "[B]le";
  (*c2_b_info)[3].fileLength = 0U;
  (*c2_b_info)[3].fileTime1 = 0U;
  (*c2_b_info)[3].fileTime2 = 0U;
  (*c2_b_info)[4].context = "";
  (*c2_b_info)[4].name = "ge";
  (*c2_b_info)[4].dominantType = "double";
  (*c2_b_info)[4].resolved = "[B]ge";
  (*c2_b_info)[4].fileLength = 0U;
  (*c2_b_info)[4].fileTime1 = 0U;
  (*c2_b_info)[4].fileTime2 = 0U;
  (*c2_b_info)[5].context = "";
  (*c2_b_info)[5].name = "cos";
  (*c2_b_info)[5].dominantType = "double";
  (*c2_b_info)[5].resolved =
    "[ILX]$matlabroot$/toolbox/eml/lib/matlab/elfun/cos.m";
  (*c2_b_info)[5].fileLength = 324U;
  (*c2_b_info)[5].fileTime1 = 1203447950U;
  (*c2_b_info)[5].fileTime2 = 0U;
  (*c2_b_info)[6].context =
    "[ILX]$matlabroot$/toolbox/eml/lib/matlab/elfun/cos.m";
  (*c2_b_info)[6].name = "nargin";
  (*c2_b_info)[6].dominantType = "";
  (*c2_b_info)[6].resolved = "[B]nargin";
  (*c2_b_info)[6].fileLength = 0U;
  (*c2_b_info)[6].fileTime1 = 0U;
  (*c2_b_info)[6].fileTime2 = 0U;
  (*c2_b_info)[7].context =
    "[ILX]$matlabroot$/toolbox/eml/lib/matlab/elfun/cos.m";
  (*c2_b_info)[7].name = "isa";
  (*c2_b_info)[7].dominantType = "double";
  (*c2_b_info)[7].resolved = "[B]isa";
  (*c2_b_info)[7].fileLength = 0U;
  (*c2_b_info)[7].fileTime1 = 0U;
  (*c2_b_info)[7].fileTime2 = 0U;
  (*c2_b_info)[8].context =
    "[ILX]$matlabroot$/toolbox/eml/lib/matlab/elfun/cos.m";
  (*c2_b_info)[8].name = "eml_scalar_cos";
  (*c2_b_info)[8].dominantType = "double";
  (*c2_b_info)[8].resolved =
    "[ILX]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_cos.m";
  (*c2_b_info)[8].fileLength = 602U;
  (*c2_b_info)[8].fileTime1 = 1209330786U;
  (*c2_b_info)[8].fileTime2 = 0U;
  (*c2_b_info)[9].context =
    "[ILX]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_cos.m";
  (*c2_b_info)[9].name = "isreal";
  (*c2_b_info)[9].dominantType = "double";
  (*c2_b_info)[9].resolved = "[B]isreal";
  (*c2_b_info)[9].fileLength = 0U;
  (*c2_b_info)[9].fileTime1 = 0U;
  (*c2_b_info)[9].fileTime2 = 0U;
  sf_mex_assign(&c2_m0, sf_mex_createstruct("nameCaptureInfo", 1, 10));
  for (c2_i6 = 0; c2_i6 < 10; c2_i6 = c2_i6 + 1) {
    c2_r0 = &c2_info[c2_i6];
    sf_mex_addfield(c2_m0, sf_mex_create("nameCaptureInfo", c2_r0->context, 15,
      0U, 0U, 0U, 2, 1, strlen(c2_r0->context)), "context",
                    "nameCaptureInfo", c2_i6);
    sf_mex_addfield(c2_m0, sf_mex_create("nameCaptureInfo", c2_r0->name, 15, 0U,
      0U, 0U, 2, 1, strlen(c2_r0->name)), "name",
                    "nameCaptureInfo", c2_i6);
    sf_mex_addfield(c2_m0, sf_mex_create("nameCaptureInfo", c2_r0->dominantType,
      15, 0U, 0U, 0U, 2, 1, strlen(c2_r0->dominantType)),
                    "dominantType", "nameCaptureInfo", c2_i6);
    sf_mex_addfield(c2_m0, sf_mex_create("nameCaptureInfo", c2_r0->resolved, 15,
      0U, 0U, 0U, 2, 1, strlen(c2_r0->resolved)), "resolved"
                    , "nameCaptureInfo", c2_i6);
    sf_mex_addfield(c2_m0, sf_mex_create("nameCaptureInfo", &c2_r0->fileLength,
      7, 0U, 0U, 0U, 0), "fileLength", "nameCaptureInfo",
                    c2_i6);
    sf_mex_addfield(c2_m0, sf_mex_create("nameCaptureInfo", &c2_r0->fileTime1, 7,
      0U, 0U, 0U, 0), "fileTime1", "nameCaptureInfo", c2_i6);
    sf_mex_addfield(c2_m0, sf_mex_create("nameCaptureInfo", &c2_r0->fileTime2, 7,
      0U, 0U, 0U, 0), "fileTime2", "nameCaptureInfo", c2_i6);
  }

  sf_mex_assign(&c2_nameCaptureInfo, c2_m0);
  return c2_nameCaptureInfo;
}

static const mxArray *c2_d_sf_marshall(void *chartInstanceVoid, void *c2_u)
{
  const mxArray *c2_y = NULL;
  boolean_T c2_b_u;
  const mxArray *c2_b_y = NULL;
  SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance;
  chartInstance = (SFc2_OdwroconeWahadlo_clInstanceStruct *)chartInstanceVoid;
  c2_y = NULL;
  c2_b_u = *((boolean_T *)c2_u);
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_u, 11, 0U, 0U, 0U, 0));
  sf_mex_assign(&c2_y, c2_b_y);
  return c2_y;
}

static real_T c2_emlrt_marshallIn(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance, const mxArray *c2_y, const char_T *c2_name)
{
  real_T c2_b_y;
  real_T c2_d0;
  sf_mex_import(c2_name, sf_mex_dup(c2_y), &c2_d0, 1, 0, 0U, 0, 0U, 0);
  c2_b_y = c2_d0;
  sf_mex_destroy(&c2_y);
  return c2_b_y;
}

static uint8_T c2_b_emlrt_marshallIn(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance, const mxArray *
  c2_b_is_active_c2_OdwroconeWahadlo_cl, const char_T *c2_name)
{
  uint8_T c2_y;
  uint8_T c2_u0;
  sf_mex_import(c2_name, sf_mex_dup(c2_b_is_active_c2_OdwroconeWahadlo_cl),
                &c2_u0, 1, 3, 0U, 0, 0U, 0);
  c2_y = c2_u0;
  sf_mex_destroy(&c2_b_is_active_c2_OdwroconeWahadlo_cl);
  return c2_y;
}

static void init_dsm_address_info(SFc2_OdwroconeWahadlo_clInstanceStruct
  *chartInstance)
{
}

/* SFunction Glue Code */
void sf_c2_OdwroconeWahadlo_cl_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(789851008U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2655214284U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1715020404U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(4285443330U);
}

mxArray *sf_c2_OdwroconeWahadlo_cl_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,4,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateDoubleMatrix(4,1,mxREAL);
    double *pr = mxGetPr(mxChecksum);
    pr[0] = (double)(365464869U);
    pr[1] = (double)(560540906U);
    pr[2] = (double)(3241529754U);
    pr[3] = (double)(1608092623U);
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(5);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  return(mxAutoinheritanceInfo);
}

static mxArray *sf_get_sim_state_info_c2_OdwroconeWahadlo_cl(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"y\",},{M[8],M[0],T\"is_active_c2_OdwroconeWahadlo_cl\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c2_OdwroconeWahadlo_cl_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance;
    chartInstance = (SFc2_OdwroconeWahadlo_clInstanceStruct *) ((ChartInfoStruct
      *)(ssGetUserData(S)))->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (_OdwroconeWahadlo_clMachineNumber_,
           2,
           1,
           1,
           2,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           ssGetPath(S),
           (void *)S);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          init_script_number_translation(_OdwroconeWahadlo_clMachineNumber_,
            chartInstance->chartNumber);
          sf_debug_set_chart_disable_implicit_casting
            (_OdwroconeWahadlo_clMachineNumber_,chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(_OdwroconeWahadlo_clMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);

          {
            unsigned int dimVector[1];
            dimVector[0]= 5;
            _SFD_SET_DATA_PROPS(0,1,1,0,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
                                1.0,0,"u",0,(MexFcnForType)c2_b_sf_marshall);
          }

          _SFD_SET_DATA_PROPS(1,2,0,1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,"y",0,
                              (MexFcnForType)c2_sf_marshall);
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of EML Model Coverage */
        _SFD_CV_INIT_EML(0,1,9,0,0,0,0,22,8);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,1622);
        _SFD_CV_INIT_EML_IF(0,0,395,410,425,1621);
        _SFD_CV_INIT_EML_IF(0,1,435,475,497,1616);
        _SFD_CV_INIT_EML_IF(0,2,510,565,596,1607);
        _SFD_CV_INIT_EML_IF(0,3,613,641,679,1594);
        _SFD_CV_INIT_EML_IF(0,4,700,725,772,1577);
        _SFD_CV_INIT_EML_IF(0,5,797,840,894,1556);
        _SFD_CV_INIT_EML_IF(0,6,923,1018,1087,1531);
        _SFD_CV_INIT_EML_IF(0,7,1120,1151,1226,1502);
        _SFD_CV_INIT_EML_IF(0,8,1263,1292,1378,1469);

        {
          static int condStart[] = { 439, 449, 461 };

          static int condEnd[] = { 445, 457, 474 };

          static int pfixExpr[] = { 0, 1, -3, 2, -3 };

          _SFD_CV_INIT_EML_MCDC(0,0,439,474,3,0,&(condStart[0]),&(condEnd[0]),5,
                                &(pfixExpr[0]));
        }

        {
          static int condStart[] = { 514, 524, 536, 552 };

          static int condEnd[] = { 520, 532, 548, 564 };

          static int pfixExpr[] = { 0, 1, -3, 2, -3, 3, -3 };

          _SFD_CV_INIT_EML_MCDC(0,1,514,564,4,3,&(condStart[0]),&(condEnd[0]),7,
                                &(pfixExpr[0]));
        }

        {
          static int condStart[] = { 617, 632 };

          static int condEnd[] = { 628, 640 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(0,2,617,640,2,7,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        {
          static int condStart[] = { 704, 716 };

          static int condEnd[] = { 712, 724 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(0,3,704,724,2,9,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        {
          static int condStart[] = { 801, 813, 825 };

          static int condEnd[] = { 809, 821, 839 };

          static int pfixExpr[] = { 0, 1, -3, 2, -3 };

          _SFD_CV_INIT_EML_MCDC(0,4,801,839,3,11,&(condStart[0]),&(condEnd[0]),5,
                                &(pfixExpr[0]));
        }

        {
          static int condStart[] = { 927, 939, 951, 1004 };

          static int condEnd[] = { 935, 947, 964, 1017 };

          static int pfixExpr[] = { 0, 1, -3, 2, -3, 3, -3 };

          _SFD_CV_INIT_EML_MCDC(0,5,927,1017,4,14,&(condStart[0]),&(condEnd[0]),
                                7,&(pfixExpr[0]));
        }

        {
          static int condStart[] = { 1125, 1137 };

          static int condEnd[] = { 1133, 1150 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(0,6,1125,1150,2,18,&(condStart[0]),&(condEnd[0]),
                                3,&(pfixExpr[0]));
        }

        {
          static int condStart[] = { 1267, 1279 };

          static int condEnd[] = { 1275, 1292 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(0,7,1267,1292,2,20,&(condStart[0]),&(condEnd[0]),
                                3,&(pfixExpr[0]));
        }

        _SFD_TRANS_COV_WTS(0,0,0,1,0);
        if (chartAlreadyPresent==0) {
          _SFD_TRANS_COV_MAPS(0,
                              0,NULL,NULL,
                              0,NULL,NULL,
                              1,NULL,NULL,
                              0,NULL,NULL);
        }

        {
          real_T (*c2_u)[5];
          real_T *c2_y;
          c2_y = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
          c2_u = (real_T (*)[5])ssGetInputPortSignal(chartInstance->S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, c2_u);
          _SFD_SET_DATA_VALUE_PTR(1U, c2_y);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration
        (_OdwroconeWahadlo_clMachineNumber_,chartInstance->chartNumber,
         chartInstance->instanceNumber);
    }
  }
}

static void sf_opaque_initialize_c2_OdwroconeWahadlo_cl(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc2_OdwroconeWahadlo_clInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c2_OdwroconeWahadlo_cl
    ((SFc2_OdwroconeWahadlo_clInstanceStruct*) chartInstanceVar);
  initialize_c2_OdwroconeWahadlo_cl((SFc2_OdwroconeWahadlo_clInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c2_OdwroconeWahadlo_cl(void *chartInstanceVar)
{
  enable_c2_OdwroconeWahadlo_cl((SFc2_OdwroconeWahadlo_clInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c2_OdwroconeWahadlo_cl(void *chartInstanceVar)
{
  disable_c2_OdwroconeWahadlo_cl((SFc2_OdwroconeWahadlo_clInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c2_OdwroconeWahadlo_cl(void *chartInstanceVar)
{
  sf_c2_OdwroconeWahadlo_cl((SFc2_OdwroconeWahadlo_clInstanceStruct*)
    chartInstanceVar);
}

static mxArray* sf_internal_get_sim_state_c2_OdwroconeWahadlo_cl(SimStruct* S)
{
  ChartInfoStruct *chartInfo = (ChartInfoStruct*) ssGetUserData(S);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c2_OdwroconeWahadlo_cl
    ((SFc2_OdwroconeWahadlo_clInstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
  prhs[3] = sf_get_sim_state_info_c2_OdwroconeWahadlo_cl();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

static void sf_internal_set_sim_state_c2_OdwroconeWahadlo_cl(SimStruct* S, const
  mxArray *st)
{
  ChartInfoStruct *chartInfo = (ChartInfoStruct*) ssGetUserData(S);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = mxDuplicateArray(st);      /* high level simctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c2_OdwroconeWahadlo_cl();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c2_OdwroconeWahadlo_cl((SFc2_OdwroconeWahadlo_clInstanceStruct*)
    chartInfo->chartInstance, mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static mxArray* sf_opaque_get_sim_state_c2_OdwroconeWahadlo_cl(SimStruct* S)
{
  return sf_internal_get_sim_state_c2_OdwroconeWahadlo_cl(S);
}

static void sf_opaque_set_sim_state_c2_OdwroconeWahadlo_cl(SimStruct* S, const
  mxArray *st)
{
  sf_internal_set_sim_state_c2_OdwroconeWahadlo_cl(S, st);
}

static void sf_opaque_terminate_c2_OdwroconeWahadlo_cl(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc2_OdwroconeWahadlo_clInstanceStruct*) chartInstanceVar
      )->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
    }

    finalize_c2_OdwroconeWahadlo_cl((SFc2_OdwroconeWahadlo_clInstanceStruct*)
      chartInstanceVar);
    free((void *)chartInstanceVar);
    ssSetUserData(S,NULL);
  }
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c2_OdwroconeWahadlo_cl(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c2_OdwroconeWahadlo_cl
      ((SFc2_OdwroconeWahadlo_clInstanceStruct*)(((ChartInfoStruct *)
         ssGetUserData(S))->chartInstance));
  }
}

static void mdlSetWorkWidths_c2_OdwroconeWahadlo_cl(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable("OdwroconeWahadlo_cl","OdwroconeWahadlo_cl",2);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop("OdwroconeWahadlo_cl",
                "OdwroconeWahadlo_cl",2,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop("OdwroconeWahadlo_cl",
      "OdwroconeWahadlo_cl",2,"gatewayCannotBeInlinedMultipleTimes"));
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,"OdwroconeWahadlo_cl",
        "OdwroconeWahadlo_cl",2,1);
      sf_mark_chart_reusable_outputs(S,"OdwroconeWahadlo_cl",
        "OdwroconeWahadlo_cl",2,1);
    }

    sf_set_rtw_dwork_info(S,"OdwroconeWahadlo_cl","OdwroconeWahadlo_cl",2);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
    ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  }

  ssSetChecksum0(S,(2270155837U));
  ssSetChecksum1(S,(568557826U));
  ssSetChecksum2(S,(733052879U));
  ssSetChecksum3(S,(3450077523U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
}

static void mdlRTW_c2_OdwroconeWahadlo_cl(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    sf_write_symbol_mapping(S, "OdwroconeWahadlo_cl", "OdwroconeWahadlo_cl",2);
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c2_OdwroconeWahadlo_cl(SimStruct *S)
{
  SFc2_OdwroconeWahadlo_clInstanceStruct *chartInstance;
  chartInstance = (SFc2_OdwroconeWahadlo_clInstanceStruct *)malloc(sizeof
    (SFc2_OdwroconeWahadlo_clInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc2_OdwroconeWahadlo_clInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.mdlStart = mdlStart_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c2_OdwroconeWahadlo_cl;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->S = S;
  ssSetUserData(S,(void *)(&(chartInstance->chartInfo)));/* register the chart instance with simstruct */
  if (!sim_mode_is_rtw_gen(S)) {
    init_dsm_address_info(chartInstance);
  }

  chart_debug_initialization(S,1);
}

void c2_OdwroconeWahadlo_cl_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c2_OdwroconeWahadlo_cl(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c2_OdwroconeWahadlo_cl(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c2_OdwroconeWahadlo_cl(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c2_OdwroconeWahadlo_cl_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
