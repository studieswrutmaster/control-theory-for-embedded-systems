\documentclass[10pt, a4paper]{article}

%Preambuła dokumentu
\usepackage{graphicx}       % pakiet graficzny, umożliwiający m.in.
                            % import grafik w formacie eps
\usepackage{rotating}       % pakiet umożliwiający obracanie rysunków
\usepackage{subfigure}      % pakiet umożliwiający tworzenie podrysunków
\usepackage{epic}           % pakiet umożliwiający rysowanie w środowisku latex
\usepackage{listings}       % pakiet dedykowany zrodlom programow
\usepackage{verbatim}       % pakiet dedykowany rozmaitym wydrukom tekstowym
\usepackage{amssymb}        % pakiet z rozmaitymi symbolami matematycznymi
\usepackage{amsmath}        % pakiet z rozmaitymi środowiskami matematycznymi
% \usepackage[polish]{babel}  % pakiet lokalizujący dokument w języku polskim
\usepackage[OT4]{fontenc}
\usepackage[utf8]{inputenc} % w miejsce utf8 można wpisać latin2 bądź cp1250,
                            % w zależności od tego w jaki sposób kodowane są 
                            % polskie znaki diakrytyczne przy wprowadzaniu 
                            % z klawiatury.

%\usepackage[draft]{prelim2e}% informacja w stopcje o statusie dokumentu 
                            % (draft-szkic lub final-wersja ostateczna) 

\textwidth      16cm
\textheight     25.5cm
\evensidemargin -3mm
\oddsidemargin  -3mm
\topmargin      -20mm


% deklaracje wymagane przez funkcję drukującą tytuł dokumentu:
%
\author{Łukasz Chojnacki\footnote{Wydział Elektroniki, Politechnika Wrocławska, ul. Z. Janiszewskiego 11/17, 50-372 Wrocław} 
\and 
Tomasz Łacny\footnote{Wydział Elektroniki, Politechnika Wrocławska, ul. Z. Janiszewskiego 11/17, 50-372 Wrocław}
}
 
\title{Inverted pendulum: stabilisation of an equilibrium point} 

\date{December 27, 2016}

% Koniec preambuły dokumentu

% Tekst dokumentu

\begin{document}
\maketitle % drukuje tytul, autora i datę zdefiniowaną w preambule
%
%\the\setitem
\def\tablename{Tabela}
%

\section{Introduction}
\label{sec:wstep} % definicja etykiety rozdziału
%
We are interested in a control system for an inverted pendulum that
stabilises the unstable equilibrium point of the controlled plant. In
the course of the laboratory exercise we have to design a model based
control algorithm, then investigate it through simulations, and
finally verify it on a real plant. Two model-based control strategies
are taken into account: pole-placement and linear quadratic
control. The plant model is obtained by linearisation around a
steady-state operating point of a nonlinear model that in turn is
obtained from a 3D model, created in AUTODESK Inventor. The parameters
of control algorithms have to be computed with use of Control System
Toolbox in Matlab.

\section{Linearisation of nonlinear equations of the inverted pendulum dynamics at the equilibrium}
\label{sec:pl}

Linear approximation of nonlinear equations representing a dynamics of the inverted pendulum at the equilibrium
$
x_e=
\left[
\begin{smallmatrix} 
0 & 0 & 0 & 0 
\end{smallmatrix} 
\right]^T
$,
$u_e=0$
has the form $\dot{x}=A x + B u$, where
\begin{equation}
\label{eq:AB}
 A=
 \begin{bmatrix}
     0      &   0 &   1.0000  &       0 \\ 
         0      &   0    &     0   & 1.0000 \\
   18.7931   &      0     &    0   &      0 \\
   -2.5556     &    0   &      0      &   0 \\
 \end{bmatrix} 
 \quad
 B=
 \begin{bmatrix}
 0 \\
         0\\ 
   -1.3169 \\
    0.8665 \\
 \end{bmatrix} 
\end{equation}

The eigenvalues of $A$ are following: $[ 0,   0,  4.3351, -4.3351]$. We can conclude that the
equilibrium point is unstable using the I-st Lyapunov method.

The pair $(A,B)$ is controllable because matrix $\Omega = [B\quad AB\quad A^2B\quad A^3B]$ has a full row rank that means $\text{rank}(\Omega) = n = \text{dim}(x)$. At this example $\text{rank}(\Omega)=4$ and $ n = \text{dim}(x) = 4$. Therefore it was possible to design control algorithms
stabilising the inverted pendulum at the unstable equilibrium point.


\section{Pole placement control}
\label{sec:pp}

\subsection{Control algorithm}
\label{sec:pp:as}
The control algorithm that stabilises the inverted pendulum at the equilibrium point, has the following form:
\begin{equation}
\label{eq:contr}
 u=F \cdot x + T(x) \cdot \mathrm{tgh}(10 \cdot \left[ \begin{smallmatrix} 0 & 0 & 0 & 1 \end{smallmatrix} \right] \cdot x),
\end{equation}
where
\begin{align}
 T(x) & =
 \begin{cases}
   5.1 & \mbox{ if } \left[ \begin{smallmatrix} 0 & 0 & 0 & 1 \end{smallmatrix} \right] > 0 \\
   4.3 & \mbox{ otherwise }
 \end{cases}, 
 \label{eq:T} \\
 F & = [244.3028,  100.3198,   62.2308,   66.8799] \label{eq:Fpp}
\end{align}


The matrix $F$ was determined with use of Ackerman's formula. 
The associated closed-loop characteristic polynomial has the following form
\begin{equation}
\label{eq:wh:pp}
 \mu^{*}(s)=s^4 + 24 s^3 + 216 s^2 + 864 s + 1296
\end{equation}
and its roots are following.: $[-6\quad -6\quad -6\quad -6]$

The following prerequisites were taken into account when choosing \eqref{eq:wh:pp}: $|u| < 35$ and $\lambda_i \in [-5,-9]$.

\subsection{Summary of results of simulation studies and experimental research}
\label{sec:pp:res}

We can observe in Figure~\ref{fig:stpp}
\begin{figure}[hbt]
  \setlength{\unitlength}{1.0cm}
  \centering 
  \subfigure[position of a cart (simulation)]{
    \includegraphics[width=0.23\textwidth]{./graphics/ack_pos_car_sim.eps}
  }
  \subfigure[position of a cart (experiment)]{
    \includegraphics[width=0.23\textwidth]{./graphics/ack_pos_car_exp.eps}
  } 
  \subfigure[position of a pendulum (simulation)]{
    \includegraphics[width=0.23\textwidth]{./graphics/ack_pos_pen_sim.eps}
  }
  \subfigure[position of a pendulum (experiment)]{
    \includegraphics[width=0.23\textwidth]{./graphics/ack_pos_pen_exp.eps}
  } \\
  \subfigure[velocity of a cart (simulation)]{
    \includegraphics[width=0.23\textwidth]{./graphics/ack_vel_car_sim.eps}
  }
  \subfigure[velocity of a cart (experiment)]{
    \includegraphics[width=0.23\textwidth]{./graphics/ack_vel_car_exp.eps}
  } 
  \subfigure[velocity of a pendulum (simulation)]{
    \includegraphics[width=0.23\textwidth]{./graphics/ack_vel_pen_sim.eps}
  }
  \subfigure[velocity of a pendulum (experiment)]{
    \includegraphics[width=0.23\textwidth]{./graphics/ack_vel_pen_exp.eps}
  }\\
  \subfigure[force (simulation)]{
    \includegraphics[width=0.25\textwidth]{./graphics/ack_force_sim.eps}
  }
  \subfigure[force (experiment)]{
    \includegraphics[width=0.25\textwidth]{./graphics/ack_force_exp.eps}
  }
  \caption{Results of simulation and experimental studies of a control system that is based on pole-placement algorithm}
  \label{fig:stpp}
\end{figure}
that: controller try to stabilize the system at the point $[0\quad0\quad0\quad0]$ and control value is between $[-35,35]$ at simulation and experiment. During the last 20$s$ the system has been destabilized and it is possible to observe big values of force and velocities.

The most likely causes of discrepancies are following: 
\begin{itemize}
\item uncertain model e.g. friction, mass and inertia parameters of the system.
\item disturbances e.g noise at the control and output signal.
\end{itemize}

\section{Linear-quadratic control}
\label{sec:lq}

\subsection{Control algorithm}
\label{sec:lq:as}
The control algorithm that stabilises the inverted pendulum at the equilibrium point, has the following form~\eqref{eq:contr}, \eqref{eq:T}, where 
\begin{equation}
 F =[  137.6385,   14.1421,   37.4537,   23.5732]\label{eq:Flq}
\end{equation}

The matrix $F$ was determined in the following way:
\begin{equation}
\label{eq:Flq1}
 F= -R^{-1} \cdot B^T\cdot P, \\
\end{equation}
where $P$ is a semi-positive matrix, the solution of the Algebraic Riccati Equation~\eqref{eq:ARE}:
%
\begin{equation}
\label{eq:ARE}
A^TP + PA-PBR^{-1}B^TP+Q=0. \\
\end{equation}
%
The parameters $Q$ i $R$  were chosen as follows:
%
\begin{align}
 Q & = I_4
\\
 R & = 0.005
\end{align}
%
The following prerequisites were taken into account when choosing the above parameters:$|u| < 35$, $Q=I_4$ and $R \in [0.001,0.1]$.

\subsection{Summary of results of simulation studies and experimental research}
\label{sec:lq:res}

We can observe in Figure~\ref{fig:stlq}
\begin{figure}[hbt]
  \setlength{\unitlength}{1.0cm}
  \centering 
  \subfigure[position of a cart (simulation)]{
    \includegraphics[width=0.23\textwidth]{./graphics/lqr_pos_car_sim.eps}
  }
  \subfigure[position of a cart (experiment)]{
    \includegraphics[width=0.23\textwidth]{./graphics/lqr_pos_car_exp.eps}
  } 
  \subfigure[position of a pendulum (simulation)]{
    \includegraphics[width=0.23\textwidth]{./graphics/lqr_pos_pen_sim.eps}
  }
  \subfigure[position of a pendulum (experiment)]{
    \includegraphics[width=0.23\textwidth]{./graphics/lqr_pos_pen_exp.eps}
  } \\
  \subfigure[velocity of a cart (simulation)]{
    \includegraphics[width=0.23\textwidth]{./graphics/lqr_vel_car_sim.eps}
  }
  \subfigure[velocity of a cart (experiment)]{
    \includegraphics[width=0.23\textwidth]{./graphics/lqr_vel_car_exp.eps}
  } 
  \subfigure[velocity of a pendulum (simulation)]{
    \includegraphics[width=0.23\textwidth]{./graphics/lqr_vel_pen_sim.eps}
  }
  \subfigure[velocity of a pendulum (experiment)]{
    \includegraphics[width=0.23\textwidth]{./graphics/lqr_vel_pen_exp.eps}
  }\\
  \subfigure[force (simulation)]{
    \includegraphics[width=0.25\textwidth]{./graphics/lqr_force_sim.eps}
  }
  \subfigure[force (experiment)]{
    \includegraphics[width=0.25\textwidth]{./graphics/lqr_force_exp.eps}
  }
  \caption{Results of simulation and experimental studies of a control system that is based on linear-quadratic algorithm}
  \label{fig:stlq}
\end{figure}
that controller quite good stabilize the system at the point $[0\quad0\quad0\quad0]$ and control value is between $[-35,35]$ during the simulation and the experiment.

The following prerequisites were taken into account when choosing the above parameters: \begin{itemize}
\item uncertain model e.g. friction, mass and inertia parameters of the system.
\item disturbances e.g noise at the control and output signal.
\end{itemize}

\end{document}
 
