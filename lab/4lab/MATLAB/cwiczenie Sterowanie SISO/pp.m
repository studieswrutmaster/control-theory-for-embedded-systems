function [C1, C0, D1, D0, F0]=pp(a1, a0, b0, MU)
% Funkcja wspomagaj�ca punkt 6-ty �wiczenia,
% rozwi�zuj�ca r�wnania (12) i (13).
% Zak�adamy, �e w przestrzeni roboczej zdefinowane
% s� wsp�czynniki a1, a0, b0 z r�wnania (6), wyliczone
% w punkcie 5-tym �wiczenia przy u�yciu aplikacji ident.
% Zmienna p reprezentuje wielomian (11) i mo�e by�
% zdefiniowana przy u�yciu funkcji poly, np.
% MU=poly([-20, -23, -26, -30])
% Wywo�anie
% [C1, C0, D1, D0, F0]=pp(a1,a0,b0,MU)
% wylicza wsp�czynniki transmitacji C(s) i F(s) w (9).
%
S=[  1  0  0  0; ...
    a1  1  0  0; ...
    a0 a1 b0  0; ...
     0 a0  0 b0];
m3=MU(2);
m2=MU(3);
m1=MU(4);
m0=MU(5);
M=[ m3-a1; ...
    m2-a0; ...
    m1; ...
    m0];
cd=S\M;
C1=cd(1);
C0=cd(2);
D1=cd(3);
D0=cd(4);
F0=(a0*C0+b0*D0)/(b0*D0);