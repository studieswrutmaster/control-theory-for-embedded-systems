function [c4,c3,c2,c1,c0,d4,d3,d2,d1,d0,q2,q1,q0]=pp1(a1,a0,b0,mu,q)
%
% Funkcja wspomagajaca punkt 4.1-ty cwiczenia Silnik DC: sterowanie,
% wyliczajaca parametry funkcji transmitancji C(s) (10).
% Zakladamy, ze w przestrzeni roboczej zdefinowane
% sa wspolczynniki modelu a1, a0, b0 (rc1,rc0, zc1, zc0). 
% Zmienna mu reprezentuje wielomian zadany
% zdefiniowana przy uzyciu funkcji poly, np.
% mu=poly([-5, -5, -5 -5, -5, -5]),
% natomiast zmienna 
% q jest wielomianem generatora trajektorii
% q=poly([0, 0.1*i, -0.1*i])
% Wywolanie
% [c4,c3,c2,c1,c0,d4,d3,d2,d1,d0,q2,q1,q0]=pp1(a1,a0,b0,mu,q)
% wylicza wspolczynniki transmitacji C(s).
%
%         d4*s^4 + d3*s^3 + d2*s^2 + d1*s + d0
%  C(s)=  ------------------------------------
%         c4*s^4 + c3*s^3 + c2*s^2 + c1*s + c0
%
% oraz wspolczynniki wielomianu Q(s)=s^3+q2*s^2+q1*s+q0
%

q2=q(2);
q1=q(3);
q0=q(4);

m5=mu(2);
m4=mu(3);
m3=mu(4);
m2=mu(5);
m1=mu(6);
m0=mu(7);

S=[...
             1, 0, 0, 0, 0, 0; ...
q2+a1         ,b0, 0, 0, 0, 0; ...
q1+a1*q2+a0   , 0,b0, 0, 0, 0; ...
q0+a1*q1+a0*q2, 0, 0,b0, 0, 0; ...
   a1*q0+a0*q1, 0, 0, 0,b0, 0; ...
         a0*q0, 0, 0, 0, 0,b0; ...
         ];


M=[ m5-(q2+a1); ...
    m4-(q1+a1*q2+a0); ...
    m3-(q0+a1*q1+a0*q2); ...
    m2-(   a1*q0+a0*q1); ...
    m1-(         a0*q0); ...
    m0];

lp=S\M;

l0=lp(1);
p4=lp(2);
p3=lp(3);
p2=lp(4);
p1=lp(5);
p0=lp(6);

l=[1,l0];
p=[p4, p3, p2, p1, p0];

d=p;
c=conv(q,l);

c4=c(1);
c3=c(2);
c2=c(3);
c1=c(4);
c0=c(5);
d4=d(1);
d3=d(2);
d2=d(3);
d1=d(4);
d0=d(5);
