figure(1)
plot(regPP2_yy_z_obr_min.time,regPP2_yy_z_obr_min.signals.values)
xlabel('seconds')
ylabel('rpm')
legend('wd','w')
title('experiment')
grid

figure(2)
plot(regPP2_u_PWM.time,regPP2_u_PWM.signals.values)
xlabel('seconds')
ylabel('PWM')
title('experiment')
grid

figure(3)
plot(regPP2m_yy_z_obr_min.time,regPP2m_yy_z_obr_min.signals.values)
xlabel('seconds')
ylabel('rpm')
legend('wd','w')
title('model')
grid

figure(4)
plot(regPP2m_u_PWM.time,regPP2m_u_PWM.signals.values)
xlabel('seconds')
ylabel('PWM')
title('model')
grid
